async function promesa(){
    let p = new Promise((resolve, reject) =>{
        let a = 2;
        //setTimeout se ejecuta después del el tiempo especificado en milisegundos. en este caso 2 seg.
        setTimeout(() => {
            a === 1 ? resolve('a es igual a 1.') : reject('no es igual a 1.')
        }, 2000);
    });
    
    console.log('Antes');
    
    //Con await lo que hacemos es trabajar de manera sincrona con funciones asincronas.
    try{
        const mensaje = await p;
        console.log(`En then: ${mensaje}`);
    } catch(err) {
        console.log(`En catch: ${err}`);
    }
    
    console.log('Después');
}

console.log('Antes Promesa.');
promesa();
console.log('Después Promesa');