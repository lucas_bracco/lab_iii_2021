const lista = document.getElementById('lista');
const inputTarea = document.getElementById('inputTarea');

async function leerApi(){
    lista.innerHTML = null;
    const response = await fetch('http://93.188.164.159:3000/tareas');
    const tareas =  await response.json();
    tareas.map(tarea => {
        const elemento = document.createElement('li');
        elemento.innerHTML = `${tarea.id} - ${tarea.nombre}`;
        lista.appendChild(elemento);
    });
}

async function addTarea() {
    const response = await fetch('http://93.188.164.159:3000/tareas', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            nombre: inputTarea.value
          })
    })
    if (response.ok) {
        leerApi()
      } else {
        alert('Error al agregar una tarea')
      }
}