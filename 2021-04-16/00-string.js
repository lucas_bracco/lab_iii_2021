var text1 = "'Hola'";
var text2 = '"Mundo!"';

console.log(text1 + " " + text2);

var num1 = 13;
var num2 = 40;
var text3 = 'num1 = ' + num1;

console.log(text3);

var text4 = `num1 = ${num1}, num2 = ${num2}, num1 + num2 = ${num1 + num2}`;
console.log(text4);

var tarea = `El mayor entre num1 y num2 es: ${num1 < num2 ? "num2 = " + num2 : "num1 = " + num1 }`;

console.log(tarea);