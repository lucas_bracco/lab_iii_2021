var texto1 = 'Hola mundo!!';

console.log("Minuscula: " + texto1.toLowerCase());

console.log("Mayuscula: " + texto1.toUpperCase())

console.log("texto1[3] = " + texto1[3])

var texto2 = 'Esto es un texto largo';
console.log(texto2.slice(0, 4))//Dividir la cadena desde el indice 0 (incluido) hasta el indice 4 (no incluido)
console.log(texto2.slice(-5))//Muestra los últimos 5 elementos del string.
console.log(texto2.split(' '))//Divide el string en un vector de "x" elementos divididos por el caracter seleccionado.