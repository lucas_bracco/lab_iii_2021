//EJEMPLO DE PLANTILLAS LITERALES

var persona = 'Lucas';
var edad = 28;

function miEtiqueta(strings, expPersona, expEdad){
    var str0 = strings[0];
    var str1 = strings[1];
    var str2 = strings[2];

    var strEdad;
    expEdad < 99 ? strEdad = "joven" : strEdad = "viejo";

    return `${str0}${expPersona}${str1}${strEdad}${str2}`;
}

var salida = miEtiqueta`Ese ${persona} es un ${edad}.`;
console.log(salida);