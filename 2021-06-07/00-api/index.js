const express = require('express');
const app = express();
const port = 3000;
const lista = require('./animales.json');
app.use(express.json());

app.get('/', (req, res) => {
  res.send('Hello World!');
});

// Show all animals
app.get('/animales', (req, res) => {
    res.send(lista.animales);
});

// Show animal by id
app.get('/animales/:id', (req, res) => {
    const paramId = req.params.id
    const animal = lista.animales.filter(({id}) => id ==paramId)
    res.send(animal);
});

// charge animal
app.post('/animales', (req, res) => {
    let animal = req.body;
    // console.log(lista.animales.pop().id);
    const element = lista.animales.pop()
    const id = Number(element.id) + 1;
    lista.animales.push(element);
    animal["id"] = id;
    lista.animales.push(animal);
    console.log('POST en animales');
    res.send(animal);
});

// Update animal
app.put('/animales/:id', (req, res) => {
  const editAnimal = req.body;
  const element = lista.animales.filter(({id}) => id == req.params.id);
  lista.animales.splice(element, 1, editAnimal);
  res.send('ok');
})

// Delete animal
app.delete('/animales/:id', (req, res) => {
  lista.animales = lista.animales.filter(({id}) =>  id != req.params.id);
  res.send('ok');
})

app.listen(port, () => {
  console.log(`servidor http://localhost:${port}`);
});