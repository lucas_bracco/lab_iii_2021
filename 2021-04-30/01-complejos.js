class Complejos{
    constructor(real,imaginario){
        this.real = Number(real);
        this.imaginario = Number(imaginario);
    }

    nostrar(){
        return `${this.real} + ${this.imaginario}i`
    }
}

function Sumar(){
    let real = document.getElementById("real1").value;
    let imaginario = document.getElementById("imaginario1").value;
    let num1 = new Complejos(real, imaginario);
    console.log("num1 = ",num1.nostrar());

    real = document.getElementById("real2").value;
    imaginario = document.getElementById("imaginario2").value;
    let num2 = new Complejos(real,imaginario);
    console.log("num2 = ",num2.nostrar());

    let num3 = new Complejos(num1.real + num2.real, num1.imaginario + num2.imaginario);
    console.log("num3 = ",num3.nostrar());
}