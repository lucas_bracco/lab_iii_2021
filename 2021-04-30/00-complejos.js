class Complejos{
    constructor(real,imaginario){
        this.real = real;
        this.imaginario = imaginario;
    }

    modulo(){
        return Math.sqrt(this.real**2 + this.imaginario**2);
    }
}

function Calcular(){
    let real = document.getElementById("real").value;
    let imaginario = document.getElementById("imaginario").value;
    let num1 = new Complejos(real, imaginario);
    console.log(num1.real, num1.imaginario)
    alert(num1.modulo());
}

