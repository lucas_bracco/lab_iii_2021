const lista = document.getElementById('lista');
const contador1 = document.getElementById('contador1');
const contador2 = document.getElementById('contador2');
let aux1 = 0;
let aux2 = 0;

function Agregar(){
    const elemento = document.createElement('li');

    const checkbox = document.createElement('input');
    checkbox.setAttribute('type', 'checkbox');
    checkbox.onchange = () => contador2.innerHTML = checkbox.checked ? ++aux2 : --aux2;

    const eliminar = document.createElement('button');
    eliminar.innerHTML = 'x';
    eliminar.onclick = () =>{ 
        if (checkbox.checked){
            contador2.innerHTML = --aux2;
        }
        contador1.innerHTML = --aux1;
        elemento.remove();
    }

    const contenido = document.createElement('span');
    contenido.innerHTML = document.getElementById('entrada').value;

    elemento.appendChild(checkbox);
    elemento.appendChild(contenido);
    elemento.appendChild(eliminar);
    lista.appendChild(elemento);
    aux1++;
    contador1.innerHTML = aux1;
}