var hora = new Date();

console.log(hora);

console.log(hora.toUTCString()); //Muestra la hora en el meridiano de Greenwich.

console.log(hora.toLocaleString()); //Muestra la hora en el formato local.

//FECHA
console.log(hora.toLocaleDateString());

console.log(hora.toDateString());

//HORA
console.log(hora.toTimeString());

console.log(hora.toLocaleTimeString());

//-------------------------------------------

console.log(hora.toJSON()) //Muestra la hora en formato JSON

var hora2 = new Date("2000/04/13 00:00:00"); //Marca la fecha en el año 2000.

console.log(hora2.toUTCString()); 

var hora3 = new Date(1993, 3, 13, 10, 30, 25)//(año, mes, día, hora, minuto, segundo)

console.log(hora3.toUTCString());

