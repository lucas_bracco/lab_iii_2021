var hora = new Date();

console.log(hora.getDate(), "Día."); //Muestra el día.

console.log(hora.getFullYear(), "Año."); //Muestra el año.

console.log(hora.getMonth(), "Mes."); //Muestra el índice del mes (0 = Enero / 11= Diciembre)

console.log(hora.getHours(), "Hora."); //Muestra la hora.

console.log(hora.getMinutes(), "Minutos."); //Muestra los minutos.

console.log(hora.getSeconds(), "Segundos."); //Muestra los segundos.

console.log(hora.getMilliseconds(), "Milisegundos."); //Muestra los milisegundos.

console.log(hora.getDay(), "Índice día de la semana."); //Muestra el índice del día de la semana (0 = Domingo / 7 = Sábado)

console.log(hora.getTime(), "Milisegundos desde 1970."); //Muestra los milisegundos desde 1970

console.log(hora.getUTCDate());

var hora2 = Date.now(); //Devuelve un entero (milisegundos que pasaron desde 1970).

console.log(hora2);