//Crear un objeto fecha con año, mes y día de nacimiento de una persona.

var fechaNacimiento = new Date(1993, 3, 13);

var fechaActual = new Date();

var años = fechaActual.getFullYear() - fechaNacimiento.getFullYear();

console.log(`Tiene ${años} años.`);