const express = require('express');
const app = express();
const port = 3000;
const lista = require('./animales.json');
app.use(express.json());

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.get('/animales', (req, res) => {
    res.send(lista.animales);
});

app.get('/animales/:id', (req, res) => {
    const paramId = req.params.id
    const animal = lista.animales.filter(({id}) => Number(id) === Number(paramId))
    res.send(animal);
});

app.post('/animales', (req, res) => {
    let animal = req.body;
    // console.log(lista.animales.pop().id);
    const element = lista.animales.pop()
    const id = Number(element.id) + 1;
    lista.animales.push(element);
    animal["id"] = id;
    lista.animales.push(animal);
    console.log('POST en animales');
    res.send(animal);
});

app.listen(port, () => {
  console.log(`servidor http://localhost:${port}`);
});