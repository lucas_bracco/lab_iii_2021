var Centigrado = document.getElementById("C");
var Fahrenheit = document.getElementById("F");
var Kelvin = document.getElementById("K");


function convertirC(){
    var Cnum = Number(Centigrado.value);
    Fahrenheit.value = (Cnum * 9/5) + 32;
    Kelvin.value = Cnum + 273.15;
}

function convertirF(){
    var Fnum = Number(Fahrenheit.value);
    Centigrado.value = (Fnum - 32) * 5/9;
    Kelvin.value = (Fnum - 32) * 5/9 + 273.15;
}

function convertirK(){
    var Knum = Number(Kelvin.value);
    Centigrado.value = Knum - 273.15;
    Fahrenheit.value = (Knum - 273.15) * 9/5 + 32;
}