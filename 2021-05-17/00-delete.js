const lista = document.getElementById('lista');
const inputTarea = document.getElementById('inputTarea');

async function leerApi(){
    lista.innerHTML = null;
    const response = await fetch('http://93.188.164.159:3000/tareas');
    const tareas =  await response.json();
    tareas.map(tarea => {
        const elemento = document.createElement('li');
        const texto = document.createElement('span');
        texto.innerHTML = `${tarea.id} - ${tarea.nombre}`;
        const eliminar = document.createElement('button');
        eliminar.innerHTML = 'x';
        const checkbox = document.createElement('input');
        checkbox.setAttribute('type', 'checkbox');
        tarea.tareaLista ? checkbox.setAttribute('checked', '') : null
        checkbox.onchange = async () => {
            const response = await fetch('http://93.188.164.159:3000/tareas/' + tarea.id, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    nombre: tarea.nombre,
                    tareaLista: !tarea.tareaLista,
                })
            })

            if (response.ok) {
                console.log(`Se modificó la tarea ${tarea.nombre}`);
            } else{
                console.log(`Ocurrió un error`);
            }

        }
        eliminar.onclick = async () =>{ 
            const response = await fetch('http://93.188.164.159:3000/tareas/' + tarea.id, {
                method: 'DELETE',
            })
            if (response.ok) {
                console.log('Eliminado: ' + tarea.id + ' ' + tarea.nombre)
                elemento.remove()
              } else {
                console.log('No se pudo eliminar la tarea: ' + tarea.id + ' ' + tarea.nombre)
              }
            }
    elemento.appendChild(checkbox);
    elemento.appendChild(texto);
    elemento.appendChild(eliminar);
    lista.appendChild(elemento);
    });
}

async function addTarea() {
    const response = await fetch('http://93.188.164.159:3000/tareas', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            nombre: inputTarea.value,
            tareaLista: false,
          })
    })
    if (response.ok) {
        leerApi();
      } else {
        alert('Error al agregar una tarea')
      }
}