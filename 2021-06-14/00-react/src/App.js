import { useState } from "react";
import MyComponent from "./components/MyComponent";
import MyForm from "./components/MyForm";

function App() {

  let [count, setCount] = useState(0);

  // const increment = () => {
  //   setCount(count + 1);
  // }

  const decrement = () => {
    setCount(count - 1);
  }

    const loadUser = (user) => {
      // user.preventDefault();
      console.log(user);
    }

  return (
    <div className="App">
      <header className="App-header">
        <h1>Hola Mundo</h1>
        <MyComponent num={count}/>
        <button onClick={() => setCount(count + 1)} >Increment</button>
        <button onClick={decrement} >Decrement</button>
        <MyForm firstname='Lucas' surname='Bracco' onLoadUser={loadUser} />
      </header>
    </div>
  );
}

export default App;
