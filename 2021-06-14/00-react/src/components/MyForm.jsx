import { useState } from "react";

const MyForm = (props) => {
    const [firstname, setFirstname] = useState(props.firstname);
    const [surname, setSurname] = useState(props.surname);

    const handleInputFirstname = (event) => {
        setFirstname(event.target.value)
    }

    const handleInputSurname = (event) => {
        setSurname(event.target.value)
    }

    // const loadUser = (e) => {
    //     e.preventDefault();
    //     console.log({name: firstname});
    //     console.log({name: surname});
    //     setFirstname('');
    //     setSurname('');
    // }

    const loadUser = (e) => {
        e.preventDefault();
        props.onLoadUser({firstname, surname});
    }

    return (
        <form onSubmit={loadUser}>
            <div>
            <label>Firstname: </label>
            <input type="text" name='firstname' id='firstname' value={firstname} onChange={handleInputFirstname} />
            </div>
            <div>
            <label>Surname: </label>
            <input type="text" name='surname' id='surname' value={surname} onChange={handleInputSurname} />
            </div>
            <button type='submit' >Upload</button>
        </form>
    );
}
 
export default MyForm;