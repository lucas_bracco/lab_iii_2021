// const ops = require('./operaciones');

// console.log(ops.suma(3, 5));

// console.log(ops.resta(5, 3));

const { suma, resta } = require('./operaciones');

console.log(suma(3, 5));
console.log(resta(8, 10));

let obj = {
    'nombre': 'Lucas',
    'apellido': 'Bracco',
    'edad': 28,
}

const { nombre, edad } = obj;

console.log(nombre);
console.log(edad);