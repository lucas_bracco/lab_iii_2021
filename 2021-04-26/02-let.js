function contador(){
    let num = 0;

    function inc(){
        num++;
    }

    function dec(){
        num--;
    }

    function mostrar(){
        console.log(num);
    }

    return {inc, dec, mostrar} // Devuelve un objeto.
}

//Se crean dos objetos totalmente independientes.
const contador1 = contador();
const contador2 = contador();

contador1.inc();
contador1.inc();
contador1.inc();
contador1.dec();
contador1.mostrar();
contador1.dec();
contador1.dec();
contador2.mostrar();
