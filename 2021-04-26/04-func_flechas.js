// Función común.
function incremental(a){
    return a + 1;
}
console.log(incremental(10));

// Función anonima.
const inc1 = function(a){
    return a + 1;
}
console.log(inc1(15));

// Funciones flecha.
const inc2 = (a) => {
    return a + 1;
}
console.log(inc2(20));

// Funciona igual que la anterior.
const inc3 = (a) => a + 1;// Se puede omitir el return y las llaves si es solo eso.
console.log(inc3(25));

// Funciona igual que las dos anteriores.
const inc4 = a => a + 1;// Si es un solo parametro se puede omitir los parentesis.
console.log(inc4(30));