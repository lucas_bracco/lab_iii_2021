// Let solo se puede acceder dentro del bloque donde fue creada.
function Hola(){
    let mensaje = 'Hola mundo.';
    return function Fn_hola(){
        console.log(mensaje);
    }
}

const func = Hola();

func();