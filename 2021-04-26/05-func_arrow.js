const obj1 = {
    name: 'nombre1',
    func: function(){
        console.log(this);
    }
}

const obj2 = {
    name: 'nombre2',
    func: () => {
        console.log(this);
    }
}

obj1.func();
obj2.func();