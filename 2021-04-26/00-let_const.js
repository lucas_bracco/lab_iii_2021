//CONSTANTES
const constante = 'Hola Mundo'; // Las constantes no pueden ser modificadas.
console.log(`const = ${constante}`);

//VARIABLES LOCALES Y GLOBALES.
var a = 5;
var b = 4;

if(a === 5){
    var a = 40;
    let b = 20; // Las variables let solo funcionan dentro del bloque en la que son declaradas.
    console.log(a);
    console.log(b);
}

console.log(a);
console.log(b);