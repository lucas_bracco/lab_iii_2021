const express = require('express');
const app = express();
const port = 3000;
const db = require('./database')
app.use(express.json());

app.get('/', (req, res) => {
  res.send('Hello World!');
});

// Show all tasks.
app.get('/tareas', (req, res) => {
  const sql = 'SELECT * FROM tareas';
  const params = [];
  db.all(sql, params, function (err, filas){
    if(err){
      console.error('Error al leer tareas. ', err);
      res.send({message: 'Error ' + err.message})
      return
    }else{
      res.send({
        message: 'ok',
        data: filas
      });
    }
  });
});

// Show task by id.
app.get('/tareas/:id', (req, res) => {
  const sql = 'SELECT * FROM tareas WHERE id = ?';
  const params = [req.params.id];
  db.all(sql, params, (err, filas) => {
    if(err){
      console.error('Error al leer tareas. ', err);
      res.send({message: 'Error ' + err.message})
      return
    }else{
      if (filas){
        res.send({
          message: 'ok',
          data: filas
        });
      }else{
        res.send({
          message: 'No existe.',
        });
      }
    }
  });
});

// charge animal
app.post('/tareas', (req, res) => {
    console.log('POST en /tareas');
    const sql = 'INSERT INTO tareas (nombre, lista) VALUES (?,?)';
    const params = [req.body.nombre, req.body.lista];
    db.run(sql, params, (err) =>{
      if(err){
        console.error('Error al crear tarea. ', err);
        return
      }else{
        const data = {
          id: this.lastID,
          nombre: req.body.nombre,
          lista: req.body.lista
        };
        res.send({
          message: 'ok',
          data: data
        });
      }
    });
});

// Update animal
app.put('/tareas/:id', (req, res) => {
  const sql = 'UPDATE tareas SET nombre = ?, lista = ? WHERE id = ?';
  const params = [req.body.nombre, req.body.lista, req.params.id];
  db.run(sql, params, (err) =>{
    if(err){
      console.error('Error al editar tarea. ', err);
      return
    }else{
      const data = {
        id: req.params.id,
        nombre: req.body.nombre,
        lista: req.body.lista
      };
      res.send({
        message: 'ok',
        data: data
      });
    }
  });
})

// Delete animal
app.delete('/tareas/:id', (req, res) => {
  const sql = 'DELETE FROM tareas WHERE id = ?';
  const params = [req.params.id];
  db.run(sql, params, (err) =>{
    if(err){
      console.error('Error al editar tarea. ', err);
      return
    }else{
      res.send({
        message: 'ok',
      });
    }
  });
})

app.listen(port, () => {
  console.log(`servidor http://localhost:${port}`);
});